# Acronimify - voices of nuclear 

## Description
Set of acronyms for the addon Acronimify

https://addons.mozilla.org/fr/firefox/addon/acronymify/

You can download Acronymify from the following stores:

    Firefox: AMO.
    Chrome, Brave, Kiwi, Vivaldi, and other Chromium-based browsers: CWS.
    Edge: Microsoft Edge Add-ons.
    Opera: Opera addons.


## Usage
to use with the browser addon Acronimify

## Support
https://www.voix-du-nucleaire.org/
https://mastodon.cipherbliss.com/@voicesofnuclear


## Roadmap
add all acronyms from the voices of nuclear wiki

## Contributing
Do MR, these are welcome.

## Authors and acknowledgment
tykayn, voices of nuclear

## License
AGPLv3+

## Project status 
